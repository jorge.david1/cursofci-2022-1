import numpy as np

class colision:
    
    def __init__(self, r,R,N):
        self.r = r
        self.R = R
        self.N = N
    
    def probabilidad(self):
        a=0.0
        b=0.0
        while b<self.N:
            x1, y1 = np.random.uniform(-self.R,self.R),np.random.uniform(-self.R,self.R)
            x2, y2 = np.random.uniform(-self.R,self.R),np.random.uniform(-self.R,self.R)
            if x1**2 + y1**2 <= (self.R - self.r)**2 and x2**2 + y2**2 <= (self.R - self.r)**2:
                if np.sqrt((x2 - x1)**2 + (y2 - y1)**2) <= 2*self.r:
                    a = a+1.0
                b+=1
        return a/b
