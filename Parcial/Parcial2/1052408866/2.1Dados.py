
import random
import matplotlib.pyplot as plt


##2.1)MONTECARLO (Dados)

## PRIMERO SE REALIZA PARA N = 2 , N = #DADOS.


S2 = []   ## almacena todas la sumas. ( micro estado posibles usando 2 dados)



for  i in  range(10000000):

  nod1=  random.randint(1,6)
  nod2 = random.randint(1,6)

  suma = nod1 + nod2

  S2.append(suma)
  
print(" El promedio de suma es :", np.mean(S2))

plt.hist(S1)  # GRAFICA HISTOGRAMA: sigue distribució normal.

## Para 3 dados:

S3 = []  # almacena macroestados posibles (valores de suma posibles)

for  i in  range(10000000):  ## Se realiza el experimento 10 millones de veces.

  nod1=  random.randint(1,6)
  nod2 = random.randint(1,6)
  nod3 = random.randint(1,6)

  suma = nod1 + nod2 + nod3

  S3.append(suma)

print("El promedio de suma es:",np.mean(S3))

plt.hist(S3) # Grafica histograma suma de 3 dados.

## para 4 dados:

S4 = []  # almacena macroestados posibles (valores de suma posibles)

for  i in  range(10000000):

  nod1=  random.randint(1,6)
  nod2 = random.randint(1,6)
  nod3 = random.randint(1,6)
  nod4 = random.randint(1,6)

  suma = nod1 + nod2 + nod3 + nod4

  S4.append(suma)

print(" El promedio de suma es:", np.mean(S4))
  
plt.hist(S4)


## ANALISIS.

## Los histogramas muestran que los macroestados posibles. ( valor de suma) en los 
## 3 casos para N= 2,3,4 siguen una distribución normal. Donde cerca al valor medio
# se encuentra el promedio que en teoría deberia obtenerse en la suma de N = 2,3,4
# dados . los resultados muestran que para N=2 en promedio al lanzarse los dados 10 millones de 
# veces , el valor mas esperado en la suma es de 7, siguiendo la misma logica , para N= 3, 10Y N = 4
# la suma esperada es 14.


# Para el caso en que se realiza el lanzamiento de los dados simultaneamente, los resultados esperados
# serían similarmente al obtenerlos seguidos, pues los eventos son independientes e invariantes si
# se lanzan los dados al tiempo o no. el resultado obtenido primero o despúes no afectan la suma.

  

