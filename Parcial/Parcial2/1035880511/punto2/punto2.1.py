import numpy as np
import matplotlib.pyplot as plt


# Considere un sistema de N dados con N = 2,3 y 4. Para cada N determine la distribución de los resultados de la suma de los valores de los N dados graficando el número de microestados posibles (combinaciones)
#asociado a cada valor de suma posible (macroestado)

n = 10000 # Número de experimentos

#--------------------------------------------------------------------------------------------------------------------
# N=2

# Se crea lista que contiene los lanzaminetos obtenidos
dado_1 = []
dado_2 = []

# Generación de número aleatorio por lanzamiento
for k in range(0,n):
    dado_1.append(np.random.randint(1,7))
    dado_2.append(np.random.randint(1,7))

cont1 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado_1)):
    cont1.append(dado_1[i] + dado_2[i])

array_cont1 = np.asarray(cont1)
max_cont1 = np.max(array_cont1)

# Histograma
plt.hist(cont1,density=False, bins=11)
plt.axvline(array_cont1.mean(), color='k', linestyle='dashed', linewidth=1,label= 'media')
plt.title("Histograma para N = 2")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()
plt.show()

# -----------------------------------------------------------------------------------------------------------------
# N = 3

# Se crea lista que contiene los lanzaminetos obtenidos
dado__1 = []
dado__2 = []
dado__3 = []

# Generación de número aleatorio por lanzamiento
for k in range(0,n):
    dado__1.append(np.random.randint(1,7))
    dado__2.append(np.random.randint(1,7))
    dado__3.append(np.random.randint(1,7))

cont2 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado_1)):
    cont2.append(dado__1[i] + dado__2[i]+ dado__3[i])

array_cont2 = np.asarray(cont2)
max_cont2 = np.max(array_cont2)

# Histograma
plt.hist(cont2,density=False, bins=11)
plt.axvline(array_cont2.mean(), color='k', linestyle='dashed', linewidth=1,label= 'media')
plt.title("Histograma para N = 3")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()
plt.show()

# -----------------------------------------------------------------------------------------------------------------
# N = 4

# Se crea lista que contiene los lanzaminetos obtenidos
dado___1 = []
dado___2 = []
dado___3 = []
dado___4 = []

# Generación de número aleatorio por lanzamiento
for k in range(0,n):
    dado___1.append(np.random.randint(1,7))
    dado___2.append(np.random.randint(1,7))
    dado___3.append(np.random.randint(1,7))
    dado___4.append(np.random.randint(1,7))

cont3 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado_1)):
    cont3.append(dado___1[i] + dado___2[i] + dado___3[i] + dado___4[i])

array_cont3 = np.asarray(cont3)
max_cont3 = np.max(array_cont3)

# Histograma
plt.hist(cont3,density=False, bins=11)
plt.axvline(array_cont3.mean(), color='k', linestyle='dashed', linewidth=1, label= 'media')
plt.title("Histograma para N = 4")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()
plt.show()



