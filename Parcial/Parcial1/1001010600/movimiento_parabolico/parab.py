import numpy as np
import matplotlib.pyplot as plt

class yMov:

    #Metodo constructor
    def __init__(self, y0, ang, v0, ay, t):
        #Inicializa los parametros
        self.p0 = y0
        self.ang = ang*(np.pi/180) #Convierte el angulo a radianes
        self.v = v0 * np.sin(self.ang)
        self.a = ay
        self.t = t

    def Vel(self, t): #Velocidad en el respectivo eje
        return self.v + self.a * self.t

    def tVuelo(self): #Tiempo de vuelo de la particula
        return (1/self.a) * (self.v + np.sqrt( self.v ** 2 + 2*self.a*self.p0 ))
    
    def Pos(self): #Calcula las posiciones de la particula
        t = np.linspace(0, self.tVuelo(), 1000)
        return self.p0 + self.v * t - 0.5 * self.a * (t**2)

    def maxAlc(self):
        return max(self.Pos())
    
    #Grafica de la trayectoria en y respecto al tiempo

    def Trayectory(self):
        t = np.linspace(0, self.tVuelo(), 1000)
        plt.plot(t, self.Pos(), color = 'crimson')
        plt.xlabel('t')
        plt.title('Trayectoria')
        plt.savefig("fig.png")

class xMov(yMov):
    def __init__(self, x0, ang, v0, ax, t):
        #Inicializa los parametros
        self.p0 = x0
        self.ang = ang*(np.pi/180) #Convierte el angulo a radianes
        self.v = v0 * np.cos(self.ang)
        self.a = ax
        self.t = t
