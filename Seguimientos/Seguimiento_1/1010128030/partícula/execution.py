from seguimiento import particula

if __name__ == '__main__':
    #Defina su evaluación en el siguiente orden (energía, ángulo, masa, campo, carga, iteraciones, paso)
    electron = particula(energia = 18.6, angulo = 30, masa = 9.11e-31, campo = 600, carga = 1.602e-19, iteraciones = 10000, paso = 0.01)
    
    electron.grafica()
