import numpy as np
class particula:

    def __init__(self, Ek, m, q, theta, B): #Inicializamos la clase.
        self.Ek=Ek
        self.m=m
        self.q=q
        self.theta=theta
        self.B=B
    def v(self):
        return ((2*self.Ek)/self.m)**0.5 #Definimos la velocidad.
    